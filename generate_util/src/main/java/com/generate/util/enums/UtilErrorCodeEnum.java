package com.generate.util.enums;

import com.generate.util.base.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 工具类错误码枚举类
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
@Getter
@AllArgsConstructor
public enum UtilErrorCodeEnum implements ErrorCode {
    /**
     * 工具类-httpUtil-异常
     */
    UTIL_ERROR_CODE_100001("100001", "工具类-httpUtil-异常"),;

    /**
     * 错误码
     */
    private String errorCode;


    /**
     * 异常描述
     */
    private String errorDesc;

}
