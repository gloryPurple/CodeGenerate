package com.generate.util.base;

/**
 * ${title}
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
public interface ErrorCode {
    /**
     * 获取错误码
     *
     * @return 错误编码
     */
    String getErrorCode();

    /**
     * 获取错误描述
     *
     * @return 错误描述
     */
    String getErrorDesc();
}
