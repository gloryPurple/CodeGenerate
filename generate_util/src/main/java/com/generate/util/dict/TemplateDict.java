package com.generate.util.dict;

/**
 * ${desc}
 * <p>
 * User: purple Date: 2017/11/17 Version: 1.0
 * </p>
 */
public class TemplateDict {
    public final static String PACKAGE_PATH = "packagePath";
    public final static String CLASS_NAME = "className";
    public final static String DTO_NAME = "dtoName";
    public final static String AUTHOR = "author";
    public final static String DATE = "Date";
    public final static String COLUMN = "column";
    public final static String COLUMN_NAME = "columnName";
    public final static String PROPERTY_NAME = "propertyName";
    public final static String REMARKS = "remarks";
    public final static String TYPE = "type";
    public final static String TABLE_NAME = "tableName";
}
