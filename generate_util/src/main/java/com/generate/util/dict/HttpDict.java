package com.generate.util.dict;

/**
 * http常用字典
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
public final class HttpDict {
    /**
     * json格式
     */
    public static final String FORMAT_JSON = "text/json";
    /**
     * json格式
     */
    public static final String FORMAT_XML = "text/xml";
    /**
     * html格式
     */
    public static final String FORMAT_HTML = "text/html";
    /**
     * 头部属性
     */
    public static final String HEAD_CONTENT = "Content-Type";
    private HttpDict() {

    }
}
