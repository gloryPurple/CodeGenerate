package com.generate.util.dict;

/**
 * 公共常量
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
public final class CommonDict {
    /**
     * 空字符串
     */
    public static final String EMPTY_STR = "";
    /**
     * 字符集 UTF-8
     */
    public static final String UTF_8 = "UTF-8";
    /**
     * 字符集 GBK
     */
    public static final String GBK = "GBK";
    /**
     * 日期根式 yyyyMMddHHmmss
     */
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyyMMddHHmmss";
    /**
     * 日期根式 yyyyMMddHHmmssSS
     */
    public static final String YYYY_MM_DD_HH_MM_SS_SS = "yyyyMMddHHmmssSS";
    /**
     * 日期根式 yyMMddHHmmssSS
     */
    public static final String YY_MM_DD_HH_MM_SS_SS = "yyMMddHHmmssSS";
    /**
     * 日期根式 yyMMddHHmmssSS
     */
    public static final String YY_MM_DD_HH_MM_SS = "yyMMddHHmmss";
    /**
     * 日期根式 yyyy-MM-dd HH:mm:ss
     */
    public static final String YYYY_MM_DD_BLANK_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    /**
     * 日期根式 yyyyMMdd
     */
    public static final String YYYY_MM_DD = "yyyyMMdd";
    /**
     * 日期根式 HHmmss
     */
    public static final String HH_MM_SS = "HHmmss";
    /**
     * 日期根式 ssSS
     */
    public static final String SS_SS = "ssSS ";
    private CommonDict() {

    }
}
