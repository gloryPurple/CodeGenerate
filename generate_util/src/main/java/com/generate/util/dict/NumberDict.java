package com.generate.util.dict;

/**
 * 数字字典
 * <p>
 * User: 蒋文哲 Date: 2017/5/22 Version: 1.0
 * </p>
 */
public final class NumberDict {

    /**
     * 数字0
     */
    public static final int MINUS_ONE = -1;

    /**
     * 数字0
     */
    public static final int ZERO = 0;
    /**
     * 数字1
     */
    public static final int ONE = 1;
    /**
     * 数字2
     */
    public static final int TWO = 2;
    /**
     * 数字3
     */
    public static final int THREE = 3;
    /**
     * 数字4
     */
    public static final int FOUR = 4;
    /**
     * 数字5
     */
    public static final int FIVE = 5;
    /**
     * 数字6
     */
    public static final int SIX = 6;
    /**
     * 数字7
     */
    public static final int SEVEN = 7;
    /**
     * 数字8
     */
    public static final int EIGHT = 8;
    /**
     * 数字9
     */
    public static final int NINE = 9;
    /**
     * 数字10
     */
    public static final int TEN = 10;
    /**
     * 数字11
     */
    public static final int ELEVEN = 11;
    /**
     * 数字12
     */
    public static final int TWELVE = 12;
    /**
     * 数字13
     */
    public static final int THIRTEEN = 13;
    /**
     * 数字14
     */
    public static final int FOURTEEN = 14;
    /**
     * 数字15
     */
    public static final int FIFTEEN = 15;
    /**
     * 数字16
     */
    public static final int SIXTEEN = 16;
    /**
     * 数字17
     */
    public static final int SEVENTEEN = 17;
    /**
     * 数字18
     */
    public static final int EIGHTEEN = 18;
    /**
     * 数字19
     */
    public static final int NINETEEN = 19;
    /**
     * 数字20
     */
    public static final int TWENTY = 20;
    /**
     * 数字21
     */
    public static final int TWENTY_ONE = 21;
    /**
     * 数字22
     */
    public static final int TWENTY_TWO = 22;
    /**
     * 数字23
     */
    public static final int TWENTY_THREE = 23;
    /**
     * 数字24
     */
    public static final int TWENTY_FOUR = 24;
    /**
     * 数字25
     */
    public static final int TWENTY_FIVE = 25;
    /**
     * 数字26
     */
    public static final int TWENTY_SIX = 26;
    /**
     * 数字27
     */
    public static final int TWENTY_SEVEN = 27;
    /**
     * 数字28
     */
    public static final int TWENTY_EIGHT = 28;
    /**
     * 数字29
     */
    public static final int TWENTY_NINE = 29;
    /**
     * 数字30
     */
    public static final int THIRTY = 30;
    /**
     * 数字31
     */
    public static final int THIRTY_ONE = 31;
    /**
     * 数字32
     */
    public static final int THIRTY_TWO = 32;
    /**
     * 数字33
     */
    public static final int THIRTY_THREE = 33;
    /**
     * 数字34
     */
    public static final int THIRTY_FOUR = 34;
    /**
     * 数字35
     */
    public static final int THIRTY_FIVE = 35;
    /**
     * 数字36
     */
    public static final int THIRTY_SIX = 36;
    /**
     * 数字37
     */
    public static final int THIRTY_SEVEN = 37;
    /**
     * 数字38
     */
    public static final int THIRTY_EIGHT = 38;
    /**
     * 数字39
     */
    public static final int THIRTY_NINE = 39;
    /**
     * 数字40
     */
    public static final int FORTY = 40;
    /**
     * 数字41
     */
    public static final int FORTY_ONE = 41;
    /**
     * 数字42
     */
    public static final int FORTY_TWO = 42;
    /**
     * 数字43
     */
    public static final int FORTY_THREE = 43;
    /**
     * 数字44
     */
    public static final int FORTY_FOUR = 44;
    /**
     * 数字45
     */
    public static final int FORTY_FIVE = 45;
    /**
     * 数字46
     */
    public static final int FORTY_SIX = 46;
    /**
     * 数字47
     */
    public static final int FORTY_SEVEN = 47;
    /**
     * 数字48
     */
    public static final int FORTY_EIGHT = 48;
    /**
     * 数字49
     */
    public static final int FORTY_NINE = 49;
    /**
     * 数字50
     */
    public static final int FIFTY = 50;
    /**
     * 数字51
     */
    public static final int FIFTY_ONE = 51;
    /**
     * 数字52
     */
    public static final int FIFTY_TWO = 52;
    /**
     * 数字53
     */
    public static final int FIFTY_THREE = 53;
    /**
     * 数字54
     */
    public static final int FIFTY_FOUR = 54;
    /**
     * 数字55
     */
    public static final int FIFTY_FIVE = 55;
    /**
     * 数字56
     */
    public static final int FIFTY_SIX = 56;
    /**
     * 数字57
     */
    public static final int FIFTY_SEVEN = 57;
    /**
     * 数字58
     */
    public static final int FIFTY_EIGHT = 58;
    /**
     * 数字59
     */
    public static final int FIFTY_NINE = 59;
    /**
     * 数字60
     */
    public static final int SIXTY = 60;
    /**
     * 数字61
     */
    public static final int SIXTY_ONE = 61;
    /**
     * 数字62
     */
    public static final int SIXTY_TWO = 62;
    /**
     * 数字63
     */
    public static final int SIXTY_THREE = 63;
    /**
     * 数字64
     */
    public static final int SIXTY_FOUR = 64;
    /**
     * 数字65
     */
    public static final int SIXTY_FIVE = 65;
    /**
     * 数字66
     */
    public static final int SIXTY_SIX = 66;
    /**
     * 数字67
     */
    public static final int SIXTY_SEVEN = 67;
    /**
     * 数字68
     */
    public static final int SIXTY_EIGHT = 68;
    /**
     * 数字69
     */
    public static final int SIXTY_NINE = 69;
    /**
     * 数字70
     */
    public static final int SEVENTY = 70;
    /**
     * 数字71
     */
    public static final int SEVENTY_ONE = 71;
    /**
     * 数字72
     */
    public static final int SEVENTY_TWO = 72;
    /**
     * 数字73
     */
    public static final int SEVENTY_THREE = 73;
    /**
     * 数字74
     */
    public static final int SEVENTY_FOUR = 74;
    /**
     * 数字75
     */
    public static final int SEVENTY_FIVE = 75;
    /**
     * 数字76
     */
    public static final int SEVENTY_SIX = 76;
    /**
     * 数字77
     */
    public static final int SEVENTY_SEVEN = 77;
    /**
     * 数字78
     */
    public static final int SEVENTY_EIGHT = 78;
    /**
     * 数字79
     */
    public static final int SEVENTY_NINE = 79;
    /**
     * 数字80
     */
    public static final int EIGHTY = 80;
    /**
     * 数字81
     */
    public static final int EIGHTY_ONE = 81;
    /**
     * 数字82
     */
    public static final int EIGHTY_TWO = 82;
    /**
     * 数字83
     */
    public static final int EIGHTY_THREE = 83;
    /**
     * 数字84
     */
    public static final int EIGHTY_FOUR = 84;
    /**
     * 数字85
     */
    public static final int EIGHTY_FIVE = 85;
    /**
     * 数字86
     */
    public static final int EIGHTY_SIX = 86;
    /**
     * 数字87
     */
    public static final int EIGHTY_SEVEN = 87;
    /**
     * 数字88
     */
    public static final int EIGHTY_EIGHT = 88;
    /**
     * 数字89
     */
    public static final int EIGHTY_NINE = 89;
    /**
     * 数字90
     */
    public static final int NINETY = 90;
    /**
     * 数字91
     */
    public static final int NINETY_ONE = 91;
    /**
     * 数字92
     */
    public static final int NINETY_TWO = 92;
    /**
     * 数字93
     */
    public static final int NINETY_THREE = 93;
    /**
     * 数字94
     */
    public static final int NINETY_FOUR = 94;
    /**
     * 数字95
     */
    public static final int NINETY_FIVE = 95;
    /**
     * 数字96
     */
    public static final int NINETY_SIX = 96;
    /**
     * 数字97
     */
    public static final int NINETY_SEVEN = 97;
    /**
     * 数字98
     */
    public static final int NINETY_EIGHT = 98;
    /**
     * 数字99
     */
    public static final int NINETY_NINE = 99;
    /**
     * 数字100
     */
    public static final int ONE_HUNDRED = 100;
    /**
     * 1024
     */
    public static final int BYTE_NUM = 1024;
    /**
     * 毫秒
     */
    public static final int MILLISECOND = 1000;

    private NumberDict() {

    }
}
