package com.generate.util.dict;

/**
 * ${desc}
 * <p>
 * User: purple Date: 2017/11/17 Version: 1.0
 * </p>
 */
public class PathDict {
    public final static String DAL_RESOURCES = "/mapper";
    public final static String DAL_MAPPER = "/dal/mapper";
    public final static String DAL_MODEL = "/dal/model";
    public final static String MANAGER = "/manager";
    public final static String MANAGER_IMPL = "/manager/impl";
    public final static String BIZ = "/biz";
    public final static String BIZ_IMPL = "/biz/impl";
    public final static String SERVICE = "/service";
    public final static String SERVICE_IMPL = "/service/impl";
    public final static String WEB = "/web/controller";

}
