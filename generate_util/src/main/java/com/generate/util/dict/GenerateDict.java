package com.generate.util.dict;

/**
 * ${desc}
 * <p>
 * User: purple Date: 2017/11/16 Version: 1.0
 * </p>
 */
public class GenerateDict {

    public final static String TABLE_NAME = "TABLE_NAME";
    public final static String COLUMN_NAME = "COLUMN_NAME";
    public final static String DATA_TYPE = "TYPE_NAME";
    public final static String REMARKS = "REMARKS";
    public final static String CLASS_PATH = "/src/main/java/";
    public final static String RESOURCES_PATH = "/src/main/resources/";
    public final static String TEMPLATE_PATH = "/src/main/resources/template";
    public final static String GENERATE_PATH = "/tmp";

    public final static String DAL_RESOURCES_NAME = ".xml";
    public final static String DAL_MODEL_NAME = ".java";
    public final static String DAL_MAPPER_NAME = "Mapper.java";
    public final static String MANAGER_NAME = "Manager.java";
    public final static String MANAGER_IMPL_NAME = "ManagerImpl.java";
    public final static String BIZ_NAME = "Biz.java";
    public final static String BIZ_IMPL_NAME = "BizImpl.java";
    public final static String SERVICE_NAME = "Service.java";
    public final static String SERVICE_IMPL_NAME = "ServiceImpl.java";
    public final static String CONTROLLER_NAME = "Controller.java";

    public final static String DAL_RESOURCETEMP_TEMPLATE = "Mapper.ftl";
    public final static String DAL_MODEL_TEMPLATE = "dto.ftl";
    public final static String DAL_MAPPER_TEMPLATE = "dtoMapper.ftl";
    public final static String MANAGER_TEMPLATE = "Manager.ftl";
    public final static String MANAGER_IMPL_TEMPLATE = "ManagerImpl.ftl";
    public final static String BIZ_TEMPLATE = "BizTemplate.ftl";
    public final static String BIZ_IMPL_TEMPLATE = "BizImplTemplate.ftl";
    public final static String SERVICE_TEMPLATE = "ServiceTemplate.ftl";
    public final static String SERVICE_IMPL_TEMPLATE = "ServiceImplTemplate.ftl";
    public final static String CONTROLLER_TEMPLATE = "ControllerTemplate.ftl";
}
