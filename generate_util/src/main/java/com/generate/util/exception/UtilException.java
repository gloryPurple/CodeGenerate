package com.generate.util.exception;

import com.generate.util.base.ErrorCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 基础异常类
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
@Getter
@Setter
public class UtilException extends BaseException {

    public UtilException(ErrorCode errorCode, String extraMsg, Throwable cause) {
        super(errorCode, extraMsg, cause);
    }

    public UtilException(ErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }
}
