package com.generate.util.exception;

import com.generate.util.base.ErrorCode;

import java.text.MessageFormat;

/**
 * 基础异常类
 * <p>
 * User: 蒋文哲 Date: 2017/8/15 Version: 1.0
 * </p>
 */
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1279879878978686L;
    /**
     * 错误描述
     */
    private static final String MSG_TEMPLATE = "错误码:{0}, 描述:{1}, 异常信息:{2}";

    public BaseException(ErrorCode errorCode, String extraMsg, Throwable cause) {
        super(getMessage(errorCode, extraMsg), cause);
    }

    public BaseException(ErrorCode errorCode, Throwable cause) {
        super(getMessage(errorCode, cause.getMessage()), cause);
    }

    public BaseException(ErrorCode errorCode, String extraMessage) {
        super(getMessage(errorCode, extraMessage));
    }

    /**
     * 错误消息封装
     *
     * @param errorCode    ErrorCode
     * @param extraMessage String
     * @return String
     */
    private static String getMessage(ErrorCode errorCode, String extraMessage) {
        extraMessage = null != extraMessage ? extraMessage : "";
        return MessageFormat.format(MSG_TEMPLATE, new Object[]{errorCode.getErrorCode(), errorCode.getErrorDesc(), extraMessage});
    }
}
