package com.test.frame;

import lombok.extern.slf4j.Slf4j;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * <p>
 * User: 蒋文哲 Date: 2017/4/18 Version: 1.0
 * </p>
 */
@Slf4j
@ContextConfiguration(locations = {"classpath:com.spring.config/config-all.xml"})
public class BaseTest extends AbstractTestNGSpringContextTests {
}
