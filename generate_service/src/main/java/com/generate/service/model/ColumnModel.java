package com.generate.service.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 列model
 * <p>
 * User: purple Date: 2017/11/16 Version: 1.0
 * </p>
 */
@Getter
@Setter
@ToString
public class ColumnModel {
    /**
     * 列名称
     */
    private String columnName;
    /**
     * 列类型
     */
    private String dataType;
    /**
     * 备注
     */
    private String remarks;
}
