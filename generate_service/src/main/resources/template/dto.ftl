package ${packagePath}.dal.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.math.BigInteger;
import java.util.*;
import javax.persistence.*;

/**
* ${tableName}类
* <p>
* User: ${author} Date: ${Date}
* </p>
* @param <T>
*/
@Getter
@Setter
@ToString
@Table(name = "${tableName}")
public class TPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Integer id;

<#list column as item>
    <#if item.columnName != "id">
    /**
    * ${item.remarks}
    *
    */
    @Column(name = "${item.columnName}")
    private ${item.type} ${item.propertyName};
    </#if>
</#list>
}