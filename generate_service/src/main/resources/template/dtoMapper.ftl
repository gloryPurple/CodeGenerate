package ${packagePath}.dal.mapper;

import ${packagePath}.dal.model.TPermission;
import tk.mybatis.mapper.common.Mapper;

/**
* ${dtoName} Mapper类
* <p>
* User: ${author} Date: ${Date}
* </p>
* @param <T>
*/
public interface ${dtoName}Mapper extends Mapper<${dtoName}> {
}