package ${packagePath}.service.base.impl;

import ${packagePath}.biz.base.BaseBiz;
import ${packagePath}.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 基础service实现类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 *
 * @param <T>
 */
public class BaseServiceImpl<T> implements BaseService<T> {
    /**
     * 通用mapper
     */
    @Autowired
    private BaseBiz<T> baseBizImpl;

    @Override
    public T find(T t) {
        return baseBizImpl.find(t);
    }

    @Override
    public List<T> query(T t) {
        return baseBizImpl.query(t);
    }

    @Override
    public int del(T t) {
        return baseBizImpl.del(t);
    }

    @Override
    public int add(T t) {
        return baseBizImpl.add(t);
    }

    @Override
    public int update(T t) {
        return baseBizImpl.update(t);
    }
}
