package ${packagePath}.biz.impl;

import ${packagePath}.biz.${className}Biz;
import ${packagePath}.biz.base.impl.BaseBizImpl;
import ${packagePath}.dal.model.${dtoName};
import org.springframework.stereotype.Component;

/**
* ${className}处理实现类
* <p>
* User: ${author} Date: ${Date}
* </p>
*/
@Component
public class ${className}BizImpl extends BaseBizImpl<${dtoName}> implements ${className}Biz {

}