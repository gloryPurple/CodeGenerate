package ${packagePath}.biz;

import ${packagePath}.biz.base.BaseBiz;
import ${packagePath}.dal.model.${dtoName};

/**
* ${className}处理类
* <p>
* User: ${author} Date: ${Date}
* </p>
*/
public interface ${className}Biz extends BaseBiz<${dtoName}> {

}