package ${packagePath}.manager;

import ${packagePath}.dal.model.${dtoName};
import ${packagePath}.manager.base.BaseManager;

/**
 * ${className}数据类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 */
public interface ${className}Manager extends BaseManager<${dtoName}> {
}