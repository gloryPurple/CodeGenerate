package ${packagePath}.biz.base.impl;

import ${packagePath}.biz.base.BaseBiz;
import ${packagePath}.manager.base.BaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 基础biz实现类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 *
 * @param <T>
 */
public class BaseBizImpl<T> implements BaseBiz<T> {
    /**
     * 通用mapper
     */
    @Autowired
    private BaseManager<T> baseManagerImpl;

    @Override
    public T find(T t) {
        return baseManagerImpl.find(t);
    }

    @Override
    public List<T> query(T t) {
        return baseManagerImpl.query(t);
    }

    @Override
    public int del(T t) {
        return baseManagerImpl.del(t);
    }

    @Override
    public int add(T t) {
        return baseManagerImpl.add(t);
    }

    @Override
    public int update(T t) {
        return baseManagerImpl.update(t);
    }
}
