package ${packagePath}.service;

import ${packagePath}.dal.model.${dtoName};
import ${packagePath}.service.base.BaseService;

/**
* ${className}业务类
* <p>
* User: ${author} Date: ${Date}
* </p>
*/
public interface ${className}Service extends BaseService<${dtoName}> {
}