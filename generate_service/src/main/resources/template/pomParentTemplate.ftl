<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>forget-me-not</groupId>
    <artifactId>purple_platform</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <!--********************************************* 子模块********************************************* -->
    <modules>
        <module>purple_util</module>
        <module>purple_web</module>
        <module>purple_common</module>
        <module>purple_dal</module>
        <module>purple_manager</module>
        <module>purple_biz</module>
        <module>purple_facade</module>
        <module>purple_service</module>
        <module>purple_security</module>
        <module>purple_dispatch</module>
    </modules>
    <properties>
        <!--***************************maven配置******************************-->
        <maven-dependency-plugin>2.8</maven-dependency-plugin>
        <!--***************************系统配置********************************-->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <targetJavaProject>${basedir}/src/main/java</targetJavaProject>
        <targetMapperPackage>com.purple.dal.mapper</targetMapperPackage>
        <targetModelPackage>com.purple.dal.model</targetModelPackage>
        <targetResourcesProject>${basedir}/src/main/resources</targetResourcesProject>
        <targetXMLPackage>mapper</targetXMLPackage>
        <jdk.version>1.8</jdk.version>

        <!--***************************web层 版本号***************************-->
        <log4j-web>2.6.2</log4j-web>
        <standard>1.1.2</standard>
        <servlet-api>2.5</servlet-api>

        <!--***************************数据层 版本号***************************-->
        <mybatis-ehcache>1.0.0</mybatis-ehcache>
        <ehcache-core>2.4.4</ehcache-core>
        <oval-version>1.86</oval-version>
        <druid>1.0.20</druid>
        <mybatis.version>3.3.1</mybatis.version>
        <mapper.version>3.3.6</mapper.version>
        <mysql.version>5.1.29</mysql.version>
        <pagehelper.version>4.1.4</pagehelper.version>
        <mybatis.spring.version>1.2.4</mybatis.spring.version>
        <mybatis-generator-core>1.3.2</mybatis-generator-core>

        <!--***************************工具层 版本号***************************-->
        <poi-examples.version>3.11</poi-examples.version>
        <mail.version>1.4.5</mail.version>
        <!--***************************扩展层 版本号***************************-->
        <fastjson.version>1.2.7</fastjson.version>
        <zkclient>0.10</zkclient>
        <shiro-version>1.3.2</shiro-version>
        <dubbo-version>2.5.3</dubbo-version>
        <netty.version>3.10.5.Final</netty.version>
        <zookeeper.version>3.5.1-alpha</zookeeper.version>
        <zkclient>0.10</zkclient>
        <netty-all>4.1.9.Final</netty-all>
        <javassist>3.21.0-GA</javassist>
        <cloudinsight-sdk>0.0.1</cloudinsight-sdk>
        <disruptor>3.3.6</disruptor>
        <redisson>2.1.4</redisson>
        <!--***************************Test层 版本号***************************-->
    </properties>

    <dependencyManagement>
        <!--************************ spring_Io ***************************** -->
        <dependencies>
            <dependency>
                <groupId>io.spring.platform</groupId>
                <artifactId>platform-bom</artifactId>
                <version>Brussels-SR4</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <!--************************* maven插件 ***************************** -->
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <source>${jdk.version}</source>
                    <target>${jdk.version}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                    <fork>true</fork>
                    <executable>${JAVA_1_8_HOME}/bin/javac</executable>
                </configuration>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <!--*************** dependency相关插件 ********************* -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>${maven-dependency-plugin}</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>
