package ${packagePath}.manager.impl;

import ${packagePath}.dal.model.${dtoName};
import ${packagePath}.manager.${className}Manager;
import ${packagePath}.manager.base.impl.BaseManagerImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * ${className}数据实现类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 */
@Repository
public class ${className}ManagerImpl extends BaseManagerImpl<${dtoName}> implements ${className}Manager {
}