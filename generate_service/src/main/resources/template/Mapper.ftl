<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packagePath}.dal.mapper.${dtoName}Mapper" >
  <resultMap id="BaseResultMap" type="com.purple.dal.model.${dtoName}" >
    <!--
      WARNING - @mbggenerated
    -->
    <id column="id" property="id" jdbcType="INTEGER" />

  </resultMap>
</mapper>