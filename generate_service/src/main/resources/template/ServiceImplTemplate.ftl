package ${packagePath}.service.impl;

import ${packagePath}.dal.model.${dtoName};
import ${packagePath}.service.${className}Service;
import ${packagePath}.service.base.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
* ${className}业务实现类
* <p>
* User: ${author} Date: ${Date}
* </p>
*/
@Service
public class ${className}ServiceImpl extends BaseServiceImpl<${dtoName}> implements ${className}Service {

}