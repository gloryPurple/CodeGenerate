<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd
http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.3.xsd">
    <context:component-scan base-package="${packagePath}"/>
    <!-- 引入properties配置文件 -->
    <bean id="propertyConfigurer"
          class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="locations">
            <list>
                <value>classpath:db.properties</value>
            </list>
        </property>
    </bean>
    <bean class="net.sf.oval.integration.spring.SpringInjector"/>
    <bean id="validator" class="net.sf.oval.Validator">
        <constructor-arg>
            <list>
                <bean class="net.sf.oval.configuration.annotation.AnnotationsConfigurer">
                </bean>
            </list>
        </constructor-arg>
    </bean>
    <import resource="classpath:com.spring.config/config-db.xml"/>

</beans>