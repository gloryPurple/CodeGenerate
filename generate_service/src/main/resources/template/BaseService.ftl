package ${packagePath}.service.base;

import java.util.List;

/**
 * 基础service类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 *
 * @param <T>
 */
public interface BaseService<T> {
    /**
     * 查询单个
     *
     * @param obj T
     * @return T
     */
    T find(T obj);

    /**
     * 查询批量
     *
     * @param obj T
     * @return List<T>
     */
    List<T> query(T obj);

    /**
     * @param obj T
     * @return int
     */
    int del(T obj);

    /**
     * 添加
     *
     * @param obj T
     * @return int
     */
    int add(T obj);

    /**
     * 更新
     *
     * @param obj T
     * @return int
     */
    int update(T obj);
}
