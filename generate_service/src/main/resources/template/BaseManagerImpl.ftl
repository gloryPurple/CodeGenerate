package ${packagePath}.manager.base.impl;

import ${packagePath}.manager.base.BaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 基础manager实现类
 * <p>
 * User: ${author} Date: ${Date}
 * </p>
 *
 * @param <T>
 */
public class BaseManagerImpl<T> implements BaseManager<T> {
    /**
     * 通用mapper
     */
    @Autowired
    private Mapper<T> mapper;

    @Override
    public T find(T t) {
        return mapper.selectOne(t);
    }

    @Override
    public List<T> query(T t) {
        return mapper.select(t);
    }

    @Override
    public int del(T t) {
        return mapper.delete(t);
    }

    @Override
    public int add(T t) {
        return mapper.insertSelective(t);
    }

    @Override
    public int update(T t) {
        return mapper.updateByPrimaryKeySelective(t);
    }
}
